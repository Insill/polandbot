import requests
import common_log

logger = common_log.CommonLog("connection",20).get_logger()

class ConnectionChecker:
    def __init__(self):
        pass

    def is_connected(self):
        url = "http://www.high.fi"
        try: 
            request = requests.head(url, timeout=3)
            logger.info("Internet connection fine")
            return True
        except requests.ConnectionError as ex: 
            logger.warning("Internet connection down.")
            return False
