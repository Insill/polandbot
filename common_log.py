import logging 

class CommonLog:
    def __init__(self,name,level):
        self.logger = logging.getLogger(name)
        self.level = level
        logging.basicConfig(level=self.level, filename="bot.log", filemode="a", format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")

    def get_logger(self):
        return self.logger

