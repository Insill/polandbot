'''
Created on 02.03.2022

@author: Insill
'''

import configparser
import os
import sys
import connectionchecker
import common_log
from telegram import *
from telegram.ext import *
from telegram.error import *

this_folder = os.path.dirname(os.path.abspath(__file__))
config_filepath = os.path.join(this_folder, "config.ini")
logger = common_log.CommonLog("bot",10).get_logger()
config = configparser.ConfigParser(interpolation=None)
config.read(config_filepath,encoding="utf-8")
bot = Bot(token=config.get("API Tokens", "telegram"))

STATE = None
CITY = 1
STATUS = 2
PASSENGERS = 3
CONTACT = 4 
TIME = 5 
PUBLISH = 6
COMMENTS = 7
locale = {}

def error_callback(update,context):
    try:
        error = locale.get("error")
        update.callback_query.message.edit_text(error)
        raise context.error 
    except Unauthorized as e: 
        logger.debug(f"Exception: {e}")
    except BadRequest as e:
        logger.debug(f"Exception: {e}")
    except TimedOut as e:
        logger.debug(f"Exception: {e}")
    except NetworkError as e:
        logger.debug(f"Exception: {e}")
    except ChatMigrated as e:
        logger.debug(f"Exception: {e}")
    except TelegramError as e:
        logger.debug(f"Exception: {e}")
    finally:
        start(update,context)

def start(update, context):
    logger.debug("Start command pressed")
    context.user_data["first_name"] = update.message.chat.first_name
    if update.message.chat.last_name == None:
        context.user_data["last_name"] = ""
    else: 
        context.user_data["last_name"] = update.message.chat.last_name
    language_menu(update, context)

def language_menu(update, context):
    question = config.get("Localization All", "choose_language")
    keyboard = InlineKeyboardMarkup([[InlineKeyboardButton("\U0001F1E7 English \U0001F1E7", callback_data="english")],
                    [InlineKeyboardButton("\U0001F1F7 русский язык \U0001F1F7",
                callback_data="russian")]])
#                [InlineKeyboardButton("\U0001F1F5 język polskih \U0001F1F5",
#                callback_data="polish")],

#                [InlineKeyboardButton("\U0001F1FA украї́нська мо́ва
#                \U0001F1FA", callback_data="ukrainian")]])
    update.message.reply_text(question, reply_markup=keyboard)

def language_menu_button(update, context):
    query = update.callback_query
    query.answer()
    context.user_data["language"] = update.callback_query.data
    get_locale(context.user_data["language"])
    status_menu(update, context)
 
def get_locale(language):
    section_name = "Localization " + str.title(language)
    for item,value in config[section_name].items():
        locale[item] = value

def status_menu(update, context):
    question = locale.get("status")
    text1 = locale.get("status_button1")
    text2 = locale.get("status_button2")
    greeting = locale.get("greeting")
    keyboard = InlineKeyboardMarkup([[InlineKeyboardButton(text1, callback_data="driver")],
                [InlineKeyboardButton(text2, callback_data="passenger")]])
    update.callback_query.message.edit_text(f"{greeting}, {context.user_data['first_name']} {context.user_data['last_name']}! {question}",
        reply_markup=keyboard)

def status_menu_button(update, context):
    global STATE
    query = update.callback_query
    query.answer()
    status = update.callback_query.data
    logger.debug(status)
    context.user_data["status"] = status
    if context.user_data["status"] == "driver":
        question = locale.get("city_driver")
        update.callback_query.message.edit_text(question)       
    if context.user_data["status"] == "passenger": 
        question = locale.get("city_passenger")
        update.callback_query.message.edit_text(question)
    STATE = CITY

def ask_passengers(update,context):
    global STATE 
    try: 
        amount = update.message.text
        logger.debug(amount)
        context.user_data["passengers"] = amount
    except:
        logger.exception("Exception")
        error = locale.get("invalid_answer")
        update.message.reply.text(error)

    logger.debug(amount)
    question = locale.get("contact")
    update.message.reply_text(question)
    STATE = CONTACT

def ask_city(update,context):
    global STATE
    city = update.message.text
    context.user_data["city"] = city
    logger.debug(city)
    if context.user_data["status"] == "driver":
        question = locale.get("time_driver")
        update.message.reply_text(question)
    if context.user_data["status"] == "passenger":
        question = locale.get("time_passenger")
        update.message.reply_text(question)
    STATE = TIME

def time(update,context): 
    global STATE
    time = update.message.text 
    context.user_data["time"] = time
    logger.debug(time)
    if context.user_data["status"] == "driver":
        question = locale.get("passengers")
        update.message.reply_text(question)
        STATE = PASSENGERS
    if context.user_data["status"] == "passenger":
        question = locale.get("contact")
        update.message.reply_text(question)
        STATE = CONTACT

def ask_contact(update,context):
    global STATE
    contact = update.message.text
    context.user_data["contact"] = contact
    logger.debug(contact)
    question = locale.get("comments")
    update.message.reply_text(question)
    STATE = COMMENTS

def comments(update,context):
    global STATE 
    comments = update.message.text 
    context.user_data["comments"] = comments
    publish_menu(update,context)

def publish_menu(update,context):
    message = compose_message(update,context)
    logger.debug(message)
    text1 = locale.get("publish_button")
    text2 = locale.get("restart_button")
    question = locale.get("publish_or_start")
    summary = locale.get("summary")
    keyboard = InlineKeyboardMarkup([[InlineKeyboardButton(text1, callback_data="publish")],
                [InlineKeyboardButton(text2, callback_data="restart")]])
    update.message.reply_text(f"{summary}\n\n{message}")
    update.message.reply_text(question,reply_markup=keyboard)

def publish_menu_button(update,context):
    logger.debug("Menu buttons pressed")
    query = update.callback_query
    query.answer()
    choice = update.callback_query.data
    logger.debug(choice)

    if choice == "publish":
        logger.debug("Publish command pressed")
        message = compose_message(update,context)
        post_message(message)
        credits = locale.get("credits")
        update.callback_query.message.edit_text(credits)
    if choice == "restart":
        logger.debug("Restart command pressed")
        status_menu(update,context) 

def compose_message(update,context):
    city_driving = locale.get("city_driving")
    city_needing = locale.get("city_needing")
    city_needing2 = locale.get("city_needing2")
    passengers_amount = locale.get("passengers_amount")
    when = locale.get("when1")
    message = ""
    message += f"{context.user_data['first_name']} {context.user_data['last_name']}\n"
    message += f"#{context.user_data['language']}\n"
    if context.user_data["status"] == "driver":
        message += f"\U0001F698 #{context.user_data['status']}\U0001F698 \n" #Car
        message += f"\U0001F310 {city_driving} #{context.user_data['city']}\U0001F310 \n" #Map
    if context.user_data["status"] == "passenger":
        message += f"\U0001F6B6 #{context.user_data['status']}\U0001F6B6 \n" #Man walking
        message += f"\U0001F310 {city_needing} #{context.user_data['city']}\U0001F310 \n" #Map
    if context.user_data["status"] == "driver":
        message += f"\U0001F468 {passengers_amount} {context.user_data['passengers']} \U0001F468 \n" #People
        message += f"\U0001F55B {when} {context.user_data['time']} \U0001F55B \n" #Clock
    if context.user_data["status"] == "passenger":
        message += f"\U0001F55B {city_needing2} {context.user_data['time']} \U0001F55B \n" #Clock
    message += f"\U0001F4DE {context.user_data['contact']} \U0001F4DE \n" #Phone
    message += f"\U0001F4AC {context.user_data['comments']} \U0001F4AC"
    return message

def post_message(message):
    bot.send_message(chat_id=config.get("Configuration", "channel_id"), disable_web_page_preview=True, text=message)

def text(update, context):
    global STATE 

    if STATE == CITY: 
        return ask_city(update,context)
    if STATE == CONTACT: 
        return ask_contact(update,context)
    if STATE == PASSENGERS:
        return ask_passengers(update,context)
    if STATE == TIME:
        return time(update,context)
    if STATE == COMMENTS:
        return comments(update,context)

def main():
    conn = connectionchecker.ConnectionChecker()
    logger.info("Bot started")

    while True:
        try:
            updater = Updater(config.get("API Tokens","telegram"),use_context=True)
            dispatcher = updater.dispatcher
            dispatcher.add_handler(CommandHandler("start",start))
            dispatcher.add_handler(MessageHandler(Filters.text, text))
            dispatcher.add_handler(CallbackQueryHandler(status_menu_button, pattern="^driver$"))
            dispatcher.add_handler(CallbackQueryHandler(status_menu_button, pattern="^passenger$"))
            dispatcher.add_handler(CallbackQueryHandler(publish_menu_button, pattern="^publish$"))
            dispatcher.add_handler(CallbackQueryHandler(publish_menu_button, pattern="^restart$"))
            dispatcher.add_handler(CallbackQueryHandler(language_menu_button, pattern="^english$"))
            dispatcher.add_handler(CallbackQueryHandler(language_menu_button, pattern="^polish$"))
            dispatcher.add_handler(CallbackQueryHandler(language_menu_button, pattern="^russian$"))
            dispatcher.add_handler(CallbackQueryHandler(language_menu_button, pattern="^ukrainian$"))
            dispatcher.add_error_handler(error_callback)
            updater.start_polling()
            conn.is_connected()
            updater.idle()
        except:
            logger.exception("Exception")

main()